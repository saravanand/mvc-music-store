﻿CREATE TABLE [dbo].[New_Carts](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[CartId] [varchar](50) NOT NULL,
	[AlbumId] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[TenantId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Cart_Album]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Carts]  WITH CHECK ADD  CONSTRAINT [FK_Cart_Album] FOREIGN KEY([AlbumId])
REFERENCES [dbo].[New_Albums] ([AlbumId])
GO

ALTER TABLE [dbo].[New_Carts] CHECK CONSTRAINT [FK_Cart_Album]
GO
/****** Object:  Default [DF__Carts__TenantId__117F9D94]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Carts] ADD  DEFAULT ([dbo].[fnGetTenant]()) FOR [TenantId]
GO
CREATE NONCLUSTERED INDEX [IX_Carts_TenantId] ON [dbo].[New_Carts] 
(
	[TenantId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]