﻿CREATE TABLE [dbo].[New_OrderDetails](
	[OrderDetailId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[AlbumId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [numeric](10, 2) NOT NULL,
	[TenantId] [uniqueidentifier] NULL,
 CONSTRAINT [PK__InvoiceL__0D760AD91DE57479] PRIMARY KEY CLUSTERED 
(
	[OrderDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK__InvoiceLi__Invoi__2F10007B]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_OrderDetails]  WITH NOCHECK ADD  CONSTRAINT [FK__InvoiceLi__Invoi__2F10007B] FOREIGN KEY([OrderId])
REFERENCES [dbo].[New_Orders] ([OrderId])
GO

ALTER TABLE [dbo].[New_OrderDetails] CHECK CONSTRAINT [FK__InvoiceLi__Invoi__2F10007B]
GO
/****** Object:  ForeignKey [FK_OrderDetail_Album]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Album] FOREIGN KEY([AlbumId])
REFERENCES [dbo].[New_Albums] ([AlbumId])
GO

ALTER TABLE [dbo].[New_OrderDetails] CHECK CONSTRAINT [FK_OrderDetail_Album]
GO
/****** Object:  Default [DF__OrderDeta__Tenan__1367E606]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_OrderDetails] ADD  DEFAULT ([dbo].[fnGetTenant]()) FOR [TenantId]
GO
CREATE NONCLUSTERED INDEX [IX_OrderDetails_TenantId] ON [dbo].[New_OrderDetails] 
(
	[TenantId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]