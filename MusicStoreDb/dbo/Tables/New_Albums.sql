﻿CREATE TABLE [dbo].[New_Albums](
	[AlbumId] [int] IDENTITY(1,1) NOT NULL,
	[GenreId] [int] NOT NULL,
	[ArtistId] [int] NOT NULL,
	[Title] [nvarchar](160) NOT NULL,
	[Price] [numeric](10, 2) NOT NULL,
	[AlbumArtUrl] [nvarchar](1024) NULL,
	[TenantId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Album] PRIMARY KEY CLUSTERED 
(
	[AlbumId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK__Album__ArtistId__276EDEB3]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Albums]  WITH NOCHECK ADD  CONSTRAINT [FK__Album__ArtistId__276EDEB3] FOREIGN KEY([ArtistId])
REFERENCES [dbo].[Artists] ([ArtistId])
GO

ALTER TABLE [dbo].[New_Albums] CHECK CONSTRAINT [FK__Album__ArtistId__276EDEB3]
GO
/****** Object:  ForeignKey [FK_Album_Genre]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Albums]  WITH NOCHECK ADD  CONSTRAINT [FK_Album_Genre] FOREIGN KEY([GenreId])
REFERENCES [dbo].[Genres] ([GenreId])
GO

ALTER TABLE [dbo].[New_Albums] CHECK CONSTRAINT [FK_Album_Genre]
GO
/****** Object:  Default [DF_Album_AlbumArtUrl]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Albums] ADD  CONSTRAINT [DF_Album_AlbumArtUrl]  DEFAULT (N'/Content/Images/placeholder.gif') FOR [AlbumArtUrl]
GO
/****** Object:  Default [DF__Albums__TenantId__0F975522]    Script Date: 01/06/2015 18:54:03 ******/
ALTER TABLE [dbo].[New_Albums] ADD  DEFAULT ([dbo].[fnGetTenant]()) FOR [TenantId]
GO
CREATE NONCLUSTERED INDEX [IX_Albums_TenantId] ON [dbo].[New_Albums] 
(
	[TenantId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]