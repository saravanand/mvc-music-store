﻿
USE [master]
GO

/****** Object:  Database [SampleStore]    Script Date: 01/06/2015 18:54:01 ******/
CREATE DATABASE [SampleStore] ON  PRIMARY 
( NAME = N'SampleStore', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SampleStore.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SampleStore_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\SampleStore_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [SampleStore] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SampleStore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [SampleStore] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [SampleStore] SET ANSI_NULLS OFF
GO

ALTER DATABASE [SampleStore] SET ANSI_PADDING OFF
GO

ALTER DATABASE [SampleStore] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [SampleStore] SET ARITHABORT OFF
GO

ALTER DATABASE [SampleStore] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [SampleStore] SET AUTO_CREATE_STATISTICS ON
GO

ALTER DATABASE [SampleStore] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [SampleStore] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [SampleStore] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [SampleStore] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [SampleStore] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [SampleStore] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [SampleStore] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [SampleStore] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [SampleStore] SET  DISABLE_BROKER
GO

ALTER DATABASE [SampleStore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [SampleStore] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [SampleStore] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [SampleStore] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [SampleStore] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [SampleStore] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [SampleStore] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [SampleStore] SET  READ_WRITE
GO

ALTER DATABASE [SampleStore] SET RECOVERY SIMPLE
GO

ALTER DATABASE [SampleStore] SET  MULTI_USER
GO

ALTER DATABASE [SampleStore] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [SampleStore] SET DB_CHAINING OFF
GO

USE [SampleStore]
GO

/****** Object:  Table [dbo].[Artists]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[Artists] ON
GO

INSERT [dbo].[Artists] ([ArtistId], [Name]) VALUES (1, N'Colbie Caillat')
GO

INSERT [dbo].[Artists] ([ArtistId], [Name]) VALUES (2, N'Bryan Adams')
GO

INSERT [dbo].[Artists] ([ArtistId], [Name]) VALUES (3, N'Lady Antebellum')
GO

INSERT [dbo].[Artists] ([ArtistId], [Name]) VALUES (4, N'Clinton Sparks')
GO

INSERT [dbo].[Artists] ([ArtistId], [Name]) VALUES (5, N'Mellisa Etheridge')
GO

SET IDENTITY_INSERT [dbo].[Artists] OFF
GO

/****** Object:  Table [dbo].[Genres]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[Genres] ON
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (1, N'Rock', N'Rock and Roll is a form of rock music developed in the 1950s and 1960s. Rock music combines many kinds of music from the United States, such as country music, folk music, church music, work songs, blues and jazz.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (2, N'Jazz', N'Jazz is a type of music which was invented in the United States. Jazz music combines African-American music with European music. Some common jazz instruments include the saxophone, trumpet, piano, double bass, and drums.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (3, N'Metal', N'Heavy Metal is a loud, aggressive style of Rock music. The bands who play heavy-metal music usually have one or two guitars, a bass guitar and drums. In some bands, electronic keyboards, organs, or other instruments are used. Heavy metal songs are loud and powerful-sounding, and have strong rhythms that are repeated. There are many different types of Heavy Metal, some of which are described below. Heavy metal bands sometimes dress in jeans, leather jackets, and leather boots, and have long hair. Heavy metal bands sometimes behave in a dramatic way when they play their instruments or sing. However, many heavy metal bands do not like to do this.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (4, N'Alternative', N'Alternative rock is a type of rock music that became popular in the 1980s and became widely popular in the 1990s. Alternative rock is made up of various subgenres that have come out of the indie music scene since the 1980s, such as grunge, indie rock, Britpop, gothic rock, and indie pop. These genres are sorted by their collective types of punk, which laid the groundwork for alternative music in the 1970s.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (5, N'Disco', N'Disco is a style of pop music that was popular in the mid-1970s. Disco music has a strong beat that people can dance to. People usually dance to disco music at bars called disco clubs. The word "disco" is also used to refer to the style of dancing that people do to disco music, or to the style of clothes that people wear to go disco dancing. Disco was at its most popular in the United States and Europe in the 1970s and early 1980s. Disco was brought into the mainstream by the hit movie Saturday Night Fever, which was released in 1977. This movie, which starred John Travolta, showed people doing disco dancing. Many radio stations played disco in the late 1970s.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (6, N'Blues', N'The blues is a form of music that started in the United States during the start of the 20th century. It was started by former African slaves from spirituals, praise songs, and chants. The first blues songs were called Delta blues. These songs came from the area near the mouth of the Mississippi River.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (7, N'Latin', N'Latin American music is the music of all countries in Latin America (and the Caribbean) and comes in many varieties. Latin America is home to musical styles such as the simple, rural conjunto music of northern Mexico, the sophisticated habanera of Cuba, the rhythmic sounds of the Puerto Rican plena, the symphonies of Heitor Villa-Lobos, and the simple and moving Andean flute. Music has played an important part recently in Latin America''s politics, the nueva canción movement being a prime example. Latin music is very diverse, with the only truly unifying thread being the use of Latin-derived languages, predominantly the Spanish language, the Portuguese language in Brazil, and to a lesser extent, Latin-derived creole languages, such as those found in Haiti.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (8, N'Reggae', N'Reggae is a music genre first developed in Jamaica in the late 1960s. While sometimes used in a broader sense to refer to most types of Jamaican music, the term reggae more properly denotes a particular music style that originated following on the development of ska and rocksteady.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (9, N'Pop', N'Pop music is a music genre that developed from the mid-1950s as a softer alternative to rock ''n'' roll and later to rock music. It has a focus on commercial recording, often oriented towards a youth market, usually through the medium of relatively short and simple love songs. While these basic elements of the genre have remained fairly constant, pop music has absorbed influences from most other forms of popular music, particularly borrowing from the development of rock music, and utilizing key technological innovations to produce new variations on existing themes.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (10, N'Classical', N'Classical music is a very general term which normally refers to the standard music of countries in the Western world. It is music that has been composed by musicians who are trained in the art of writing music (composing) and written down in music notation so that other musicians can play it. Classical music can also be described as "art music" because great art (skill) is needed to compose it and to perform it well. Classical music differs from pop music because it is not made just in order to be popular for a short time or just to be a commercial success.')
GO

INSERT [dbo].[Genres] ([GenreId], [Name], [Description]) VALUES (33, N'Country', N'Country Music')
GO

SET IDENTITY_INSERT [dbo].[Genres] OFF
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetTenant]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[New_Albums]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[New_Albums] ON
GO

INSERT [dbo].[New_Albums] ([AlbumId], [GenreId], [ArtistId], [Title], [Price], [AlbumArtUrl], [TenantId]) VALUES (1, 3, 1, N'Frst album', CAST(10.00 AS Numeric(10, 2)), N'/Content/Images/placeholder.gif', N'b590cd25-3093-df11-8deb-001ec9dab123')
GO

SET IDENTITY_INSERT [dbo].[New_Albums] OFF
GO

/****** Object:  Table [dbo].[New_Orders]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[New_OrderDetails]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[New_Carts]    Script Date: 01/06/2015 18:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[Albums]    Script Date: 01/06/2015 18:54:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Orders]    Script Date: 01/06/2015 18:54:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[OrderDetails]    Script Date: 01/06/2015 18:54:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Carts]    Script Date: 01/06/2015 18:54:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
