﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace MvcMusicStore.Models
{
    public class Artist
    {
       
        public int ArtistId { get; set; }
        public string Name { get; set; }
    }
}