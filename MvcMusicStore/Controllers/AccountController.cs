﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Mvc3ToolsUpdateWeb_Default.Models;
using MvcMusicStore.Models;
using CelloSaaS.Integration.RestClients;
using CelloSaaS.Integration.Helpers;
using CelloSaaS.Integration.Modules;
using System.Net.Http;
using System.Dynamic;
using System.Net;
using System.IdentityModel.Tokens;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;
using Newtonsoft.Json;


namespace Mvc3ToolsUpdateWeb_Default.Controllers
{
    public class AccountController : Controller
    {

        static HttpClient _client;
        static WebClient client_;

        public AccountController()
        {

#if DEBUG || Test
            _client = new HttpClient(new WebRequestHandler { ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true });
#else
            _client = new HttpClient();
#endif
            client_ = new WebClient();
        }

        private const string DefaultPolicy = "GlobalExceptionLogger";
        public static string authServerUri;
        private void MigrateShoppingCart(string UserName)
        {
            // Associate shopping cart items with logged-in user
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.MigrateCart(UserName);
            Session[ShoppingCart.CartSessionKey] = UserName;
        }

        //
        // GET: /Account/LogOn

        public ActionResult LogOn(string companyCode = null, string appType = null)
        {

            authServerUri = AppSettingHelpers.GetValue<string>(AppSettingConstants.Oauth_URL);
            if (!string.IsNullOrEmpty(authServerUri))
            {

                //  data = HttpUtility.HtmlEncode(data);
                var clientUri = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

                if (string.IsNullOrEmpty(clientUri))
                {
                    ModelState.AddModelError("", "Not able to infer the requested Uri");
                    return View("CompanyCode");
                }
                try
                {
                    // cellohelp:SearchClients searchclients details using clienturi or companycode
                    var clients = ClientService.SearchClients(clientUri, authServerUri, companyCode);

                    if (clients == null || clients.Count < 1)
                        if (string.IsNullOrEmpty(companyCode))
                            return View("CompanyCode");
                        else
                        {
                            ModelState.AddModelError("", "Not able to find the client matching the given Uri");
                            return View("CompanyCode");
                        }

                    var client = clients.FirstOrDefault();

                    if (client == null)
                    {
                        ModelState.AddModelError("", "Not able to find the client matching the given Uri");
                        return View();
                    }

                    string clientTenantId = client.TryGetStringValue("TenantId");
                    var tempClientRedirUris = client.TryGetValue<string>("RedirectUris");
                    var clientRedirUris = JsonConvert.DeserializeObject<List<string>>(tempClientRedirUris);
                    Guid clientId = Guid.Parse(client.TryGetStringValue("id"));
                    string clientRedirUri = clientRedirUris[clientRedirUris.Count - 1];

                    var state = Request.Url.GetHashCode().ToString();


                    var parameters = new Dictionary<string, string>
            {
                {"response_type","code"},
                {"scope","openid email all"},
                {"client_id",clientId.ToString()},
                {"state",state},
                {"redirect_uri",clientRedirUri},
                {"max_age","60"},
            }.AsQueryParameters();

                    var redirectUri = new UriBuilder(authServerUri) { Path = "authorize", Query = parameters };
                    // cellohelp:Redirect to Authserver
                    return new RedirectResult(redirectUri.ToString());
                }
                catch (UnauthorizedAccessException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View("CompanyCode");
                }

            }
            return null;
        }




        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            authServerUri = AppSettingHelpers.GetValue<string>(AppSettingConstants.Oauth_URL);
            var clientRedirectUri = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            var logOffUri = new UriBuilder(authServerUri)
            {
                Path = "api/Authentication/LogOut"
            };

            string queryPart = null;
            var identity = System.Threading.Thread.CurrentPrincipal.Identity;

            FormsIdentity formsIdentity = identity as FormsIdentity;

            if (formsIdentity == null)
                throw new UnauthorizedAccessException();

            if (formsIdentity.Ticket == null || string.IsNullOrEmpty(formsIdentity.Ticket.UserData) || !formsIdentity.Ticket.UserData.Contains(AppSettingHelpers.GetValue<char>("CookieDataSeparator")))
                throw new UnauthorizedAccessException();

            string[] userData = formsIdentity.Ticket.UserData.Split(AppSettingHelpers.GetValue<char>("CookieDataSeparator"));

            if (userData == null || userData.Length < 2) throw new UnauthorizedAccessException();

            string clientId = userData[3];
            if (!string.IsNullOrEmpty(clientRedirectUri))
            {
                queryPart = string.Format(CultureInfo.InvariantCulture, "redirectUri={0}", clientRedirectUri);

                queryPart += string.Format(CultureInfo.InvariantCulture, "&client_id={0}", clientId);

            }

            logOffUri.Query = queryPart.TrimStart('?');

            // cellohelp:Redirect to Authserver
            return Redirect(logOffUri.Uri.ToString());


        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                //var conn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString);

                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, "question", "answer", true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    MigrateShoppingCart(model.UserName);

                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        public ActionResult AuthorizationCallBack(string code, string state)
        {
            Dictionary<string, string> userInfo;
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(state))
                return View("Logon");

            var parameters = new Dictionary<string, string>
            {
                {"grant_type","authorization_code"},
                {"redirect_uri",new UriBuilder
                                { 
                                    Host = Request.Url.Host,
                                    Scheme = Request.Url.Scheme, 
                                    Port=Request.Url.Port,
                                    Path= Url.Action("TokenCallBack","Account")}.ToString()
                                },
                {"code",code},
                {"client_id",Request.QueryString["client_id"]}
            };


            var tokenEndPointUri = new UriBuilder(authServerUri) { Path = "api/token" };


            client_.Headers[HttpRequestHeader.ContentType] = "application/json";
            var sc = SynchronizationContext.Current;

            var tempparameters = JsonConvert.SerializeObject(parameters);
            try
            {
                var response = client_.UploadString(tokenEndPointUri.Uri, "POST", tempparameters);
                if (Response == null)
                {
                    ModelState.AddModelError("", "Remote server error");
                    return View();
                }

                ExpandoObject tokenResponse = JsonConvert.DeserializeObject<ExpandoObject>(response);
                if (tokenResponse != null && !string.IsNullOrEmpty(tokenResponse.TryGetStringValue("access_token"))
                      && !string.IsNullOrEmpty(tokenResponse.TryGetStringValue("identity_token")))
                {
                    string accessToken = tokenResponse.TryGetStringValue("access_token");
                    client_.Headers = null;
                    client_.Headers["Authorization"] = string.Format(CultureInfo.InvariantCulture, "Bearer {0}", accessToken);
                    userInfo = JsonConvert.DeserializeObject<Dictionary<string, string>>(client_.DownloadString(AppSettingHelpers.GetValue<string>(AppSettingConstants.AuthServer_UserInfo)));
                    SetAuthenticationTicketInfo(userInfo["displayName"], userInfo["id"], userInfo["rights"].Split(','), userInfo["tenantId"], userInfo["client_id"]);

                }


            }
            catch (WebException we)
            {
                ModelState.AddModelError("", we.Message);
                return View();

            }



            return RedirectToAction("index", "home");
        }
        public void SetAuthenticationTicketInfo(string userName, string userId, string[] roles, string tenantId, string clientId)
        {
            //set user identity details
            var cookieData = new CookieData
            {
                userKey = userId,
                userName = userName,
                roles = roles,
                tenantId = tenantId
            };

            cookieData.loggedInUserTenantId = tenantId;

            if (!string.IsNullOrEmpty(clientId))
            {
                cookieData.sessiontenantid = clientId;
            }

            HttpCookie cookie = AuthenticationCookieHelper.GetAuthenticationCookie(cookieData);

            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public ActionResult Error()
        {
            ViewBag.Message = TempData["Status"];
            TempData["Status"] = null;

            return View();
        }

    }
}
